# module for pong

import turtle


def get_window():
    win = turtle.Screen()
    win.title("Pong by @blackroom24")
    win.bgcolor("black")
    win.setup(width=800, height=600)
    win.tracer(0)
    return win



# Score

def display_scorecard():
    scorecard = turtle.Turtle()
    scorecard.speed(0)
    scorecard.color("white")
    scorecard.penup()
    scorecard.goto(0, 260)
    scorecard.hideturtle()
    scorecard.write("Left: 0  Right: 0",
                    align="center",
                    font=("Courier", 14, "normal"))
    return scorecard


# Paddle left

def get_left_paddle():
    paddle_left = turtle.Turtle()
    paddle_left.speed(0)  # Animation speed
    paddle_left.color("white")
    paddle_left.shape("square")
    paddle_left.shapesize(stretch_wid=6, stretch_len=1)
    paddle_left.penup()  # By default turtle shows lines which we dont want
    paddle_left.goto(-350, 0)
    return paddle_left


# Paddle right


def get_right_paddle():
    paddle_right = turtle.Turtle()
    paddle_right.speed(0)
    paddle_right.color("white")
    paddle_right.shape("square")
    paddle_right.shapesize(stretch_wid=6, stretch_len=1)
    paddle_right.penup()
    paddle_right.goto(350, 0)
    return paddle_right


# Ball

def get_ball():
    ball = turtle.Turtle()
    ball.speed(0)
    ball.color("white")
    ball.shape("circle")
    ball.penup()
    ball.goto(0, 0)
    ball.dx = 0.13
    ball.dy = 0.1
    return ball


def paddle_left_up():
    y = paddle_left.ycor()
    y += 20
    paddle_left.sety(y)
    pass


def paddle_left_down():
    y = paddle_left.ycor()
    y -= 20
    paddle_left.sety(y)
    pass


def paddle_right_up():
    y = paddle_right.ycor()
    y += 20
    paddle_right.sety(y)
    pass


def paddle_right_down():
    y = paddle_right.ycor()
    y -= 20
    paddle_right.sety(y)
    pass


def key_binding():
    win.listen()
    win.onkeypress(paddle_left_up, "w")
    win.onkeypress(paddle_left_down, "s")
    win.onkeypress(paddle_right_up, "Up")
    win.onkeypress(paddle_right_down, "Down")
    pass


def main(max_score):

    bound_neg_xcor = -390
    bound_pos_xcor = 390
    bound_pos_ycor = 290
    bound_neg_ycor = -290

    score_a = 0
    score_b = 0



    while True:
        win.update()

        # Ball movement
        ball.setx(ball.xcor() + ball.dx)
        ball.sety(ball.ycor() + ball.dy)

        # Border Checking
        if ball.ycor() > bound_pos_ycor:
            ball.sety(290)
            ball.dy *= -1

        if ball.ycor() < bound_neg_ycor:
            ball.sety(-290)
            ball.dy *= -1

        if ball.xcor() < bound_neg_xcor:
            ball.dx *= -1
            score_b += 1
            scorecard.clear()
            scorecard.write("Left: {}  Right: {}"
                            .format(score_a, score_b),
                            align="center",
                            font=("Courier", 14, "normal"))

        if ball.xcor() > bound_pos_xcor:
            ball.dx *= -1
            score_a += 1
            scorecard.clear()
            scorecard.write("Left: {}  Right: {}"
                            .format(score_a, score_b),
                            align="center",
                            font=("Courier", 14, "normal"))

        glicth_fix1 = (ball.xcor() > 340 and ball.xcor() < 350)
        glicth_fix2 = (ball.ycor() < paddle_right.ycor() + 40
                       and ball.ycor() > paddle_right.ycor() - 40)

        if glicth_fix1 and glicth_fix2:
            ball.setx(340)
            ball.dx *= -1

        glicth_fix3 = (ball.xcor() < -340 and ball.xcor() > -350)
        glicth_fix4 = (ball.ycor() < paddle_left.ycor() + 40
                       and ball.ycor() > paddle_left.ycor() - 40)

        if glicth_fix3 and glicth_fix4:
            ball.setx(-340)
            ball.dx *= -1
        if (score_a or score_b) >= max_score:
            break
    pass



if __name__ == '__main__':
    win = get_window()
    ball = get_ball()
    paddle_left = get_left_paddle()
    paddle_right = get_right_paddle()
    scorecard = display_scorecard()
    key_binding()
    main(10)
