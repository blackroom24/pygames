"""Snake Game"""
import random
import tkinter as tk
from tkinter import messagebox
import pygame

global black, white, nokia_bg, color1
black = (0, 0, 0)
white = (255, 255, 255)
nokia_bg = (152, 251, 152)
color1 = (101, 23, 255)


class Cube(object):
    rows = 20
    w = 500

    def __init__(self, start, dirnx=1, dirny=0, color=black):
        self.pos = start
        self.dirnx = dirnx
        self.dirny = dirny
        self.color = color

    def move(self, dirnx, dirny):
        self.dirny = dirny
        self.dirnx = dirnx
        self.pos = (self.pos[0] + self.dirnx, self.pos[1] + self.dirny)

    def draw(self, surface, eyes=False):
        dis = self.w // self.rows
        i = self.pos[0]
        j = self.pos[1]

        pygame.draw.rect(
            surface, self.color, (i * dis + 1, j * dis + 1, dis - 2, dis - 2)
        )

        if eyes:
            centre = dis // 2
            radius = 3
            eye1_hpos = i * dis + centre - radius - 1
            eye_vpos = j * dis + 10
            eye2_hpos = i * dis + dis - radius * 2
            circleMiddle = (eye1_hpos, eye_vpos)
            circleMiddle2 = (eye2_hpos, eye_vpos)
            pygame.draw.circle(surface, white, circleMiddle, radius)
            pygame.draw.circle(surface, white, circleMiddle2, radius)


class Snake(object):
    body = []  # Creating a Empty List
    turns = {}  # A Dictionary used later

    def __init__(self, color, pos):
        self.color = color
        self.head = Cube(pos)
        self.body.append(self.head)
        self.dirnx = 0
        self.dirny = 1

    def move(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()

            keys = pygame.key.get_pressed()

            for key in keys:
                # Pygame has 0,0 on the far left top  hence
                if keys[pygame.K_LEFT]:
                    self.dirnx = -1
                    self.dirny = 0
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]
                elif keys[pygame.K_RIGHT]:
                    self.dirnx = 1
                    self.dirny = 0
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]
                elif keys[pygame.K_UP]:
                    self.dirnx = 0
                    self.dirny = -1
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]
                elif keys[pygame.K_DOWN]:
                    self.dirnx = 0
                    self.dirny = 1
                    self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]

        for index, cub in enumerate(self.body):
            p = cub.pos[:]
            if p in self.turns:
                turn = self.turns[p]
                cub.move(turn[0], turn[1])
                if index == len(self.body) - 1:
                    self.turns.pop(p)
                    # Removing so we dont accidentaly move again
            else:
                # Checking if snake is at end of window

                if cub.dirnx == -1 and cub.pos[0] <= 0:
                    cub.pos = (cub.rows - 1, cub.pos[1])
                elif cub.dirnx == 1 and cub.pos[0] >= cub.rows - 1:
                    cub.pos = (0, cub.pos[1])
                elif cub.dirny == 1 and cub.pos[1] >= cub.rows - 1:
                    cub.pos = (cub.pos[0, 0)
                elif cub.dirny == -1 and cub.pos[1] <= 0:
                    cub.pos = (cub.pos[1], cub.rows - 1)
                else:
                    cub.move(cub.dirnx, cub.dirny)

    def reset(self):
        self.head = Cube((10, 10))
        self.body = []
        self.body.append(self.head)
        self.turns = {}
        self.dirnx = 0
        self.dirny = 1

    def add_cube(self):
        tail = self.body[-1]
        dx = tail.dirnx
        dy = tail.dirny

        if dx == 1 and dy == 0:
            self.body.append(Cube((tail.pos[0] - 1, tail.pos[1])))
        elif dx == -1 and dy == 0:
            self.body.append(Cube((tail.pos[0] + 1, tail.pos[1])))
        elif dx == 0 and dy == 1:
            self.body.append(Cube((tail.pos[0], tail.pos[1] - 1)))
        elif dx == 0 and dy == -1:
            self.body.append(Cube((tail.pos[0], tail.pos[1] + 1)))

        self.body[-1].dirnx = dx
        self.body[-1].dirny = dy

    def draw(self, surface):
        for index, cub in enumerate(self.body):
            if index == 0:
                cub.draw(surface, True)  # Drawing eyes for head
            else:
                cub.draw(surface)


def redraw_win(surface):
    surface.fill(nokia_bg)
    snake.draw(surface)  # DRAW only after surface  is filled
    snake_food.draw(surface)
    pygame.display.update()


def food(rows, item):
    positions = item.body
    while True:
        x = random.randrange(rows)
        y = random.randrange(rows)
        if len(list(filter(lambda snake: snake.pos == (x, y), positions))) > 0:
            continue
        else:
            break
    return x, y


def prompt(subject, content):
    root=tk.Tk()
    root.attributes("-topmost", True)
    messagebox.showinfo(subject, content)
    root.destroy()


def main():
    global width, rows, snake, snake_food
    width=500
    rows=20
    win=pygame.display.set_mode((width, width))
    snake=Snake(black, (10, 10))
    snake_food=Cube(food(rows, snake), color=color1)
    clock=pygame.time.Clock()
    while True:
        pygame.time.delay(50)
        clock.tick(10)
        snake.move()
        if snake.body[0].pos == snake_food.pos:
            snake.add_cube()
            snake_food=Cube(food(rows=rows, item=snake), color=color1)
        for x in range(len(snake.body)):
            if snake.body[x].pos in list(map(lambda s: s.pos,
                                             snake.body[x + 1:])):
                print("Score: ", len(snake.body))
                prompt("You Lost!", "Play Agian")
                snake.reset()
                break
        redraw_win(win)


if __name__ == "__main__":
    main()
